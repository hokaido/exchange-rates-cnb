# Exchange rates CNB

Application load actual exchange rates from server CNB and show data in table. You can also set money in Czech crowns and exchange by actual rate with selected currency.

Tested on Node.js version 10.15.2.

## Important !

To test web app you must run Chrome browser with disabled security to overcome CORS issues.
Server CNB doesn't send Access-Control-Allow-Origin header.

Run chrome with disabled CORS:

__Windows:__ : `"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-web-security --user-data-dir`

__Linux__ : `google-chrome --args --disable-web-security --user-data-dir`

__Mac__: `open -a "Google Chrome" --args --disable-web-security --user-data-dir`

## How start application

* `yarn`
* `yarn start`
