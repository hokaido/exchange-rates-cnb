import * as React from 'react';

interface Props {
  rates: store.Rate[];
  onRateChange: (rate: string) => void;
}

const list = (props: Props) => {

  return (
    <select onChange={(e) => props.onRateChange(e.target.value)}>
      <option value=""></option>
      {props.rates.map((rate: store.Rate, i: number) => {
        if (rate) {
          return <option key={i} value={`${rate.rate}:${rate.amount}:${rate.code}`}>{rate.country} ({rate.code})</option>
        }
      }
      )}
    </select>
  )
}

export default list;

