
import * as React from 'react';

interface Props {
  row: store.Rate;
}

const tableRow = (props: Props) => {
  if (!props.row) return null;
  return (
    <tr>
      {Object.keys(props.row).map((key: string, i: number) => <td key={i}>{props.row[key]}</td>)}
    </tr>
  )
}

export default tableRow;