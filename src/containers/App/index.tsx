import * as React from 'react';
import * as styles from '../../styles.scss';
import { connect } from 'react-redux';

import { CNB_API } from '../../config';
import { AppState } from '../../store/reducers';
import  actionTypes from '../../store/actionTypes';
import { parseRates } from '../../utils';
import TableRow from '../../components/TableRow';
import SelectList from '../../components/SelectList';

interface Props {
  onRatesLoadedHandler: (rates: store.Rate[]) => AppState;
  onRateChangeHandler: (rate: string) => AppState;
  onSetHeader: (headerItems: string[]) => AppState;
  onSetDate: (date: string) => AppState;
  onSetMoney: (value: string) => AppState;

  rates: store.Rate[];
  rate: string;
  headerTable: string[];
  date: string;
  money: string;
}

interface State {
  amountInput: number;
}

class App extends React.Component<Props, State> {

  constructor(props: Props, state: State) {
    super(props, state);
    this.state = {
      amountInput: 0,
    }
  }

  componentDidMount = () => {
    fetch(CNB_API).then(resp => {
      return resp.text().then(values => {
        return values;
      }).then(value => {
        const {date, header, data} = parseRates(value);
        this.props.onSetHeader(header);
        this.props.onSetDate(date);
        this.props.onRatesLoadedHandler(data);
      })
    })
  }

  onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      amountInput: parseFloat(event.target.value),
    })
  }

  onSubmit = () => {
    const [rate, amount, code ] = this.props.rate.split(':');
    const result = ((this.state.amountInput / parseFloat(rate)) * parseInt(amount)).toFixed(3);
    this.props.onSetMoney(`${result} ${code}`);
  }

  render() {
    return (
      <React.Fragment>
        <h1>Kurzový lístek ke dnu: {this.props.date}</h1>
        <table>
          <thead>
            <tr>
              {this.props.headerTable.map((head: string, i: number) => <th key={i}>{head}</th>)}
            </tr>
          </thead>
          <tbody>
            {this.props.rates.map((rate: store.Rate, i: number) => <TableRow key={i} row={rate} />)}
          </tbody>
        </table>
        <div className={styles.form}>
          <label htmlFor="amountInput">Částka v CZK</label>
          <input className={styles.input} onChange={this.onInputChange}  id="amountInput" type="number"/>
          <span> do </span>
          <SelectList onRateChange={this.props.onRateChangeHandler} rates={this.props.rates} />
          <button className={styles.input} onClick={this.onSubmit}>Zmenit</button>
          {this.props.money.length > 0 && <p>Dostanete {this.props.money}</p>}
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    rates: state.rates,
    rate: state.choosenRate,
    date: state.date,
    headerTable: state.tableHeader,
    money: state.money,
  }
}

const mapDispatchToProps = (dispatch: any) => {

  return {
    onRatesLoadedHandler: (rates: store.Rate[]) => dispatch({type: actionTypes.LOAD_RATES, value: rates}),
    onRateChangeHandler: (rate: string) => dispatch({type: actionTypes.CHANGE_RATE, value: rate}),
    onSetHeader: (headerItems: string[]) => dispatch({type: actionTypes.SET_HEADER, value: headerItems}),
    onSetDate: (date: string) => dispatch({type: actionTypes.SET_DATE, value: date}),
    onSetMoney: (value: string) => dispatch({type: actionTypes.SET_MONEY, value: value}),
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(App);