enum ActionTypes {
  LOAD_RATES = 'LOAD_RATES',
  CHANGE_RATE = 'CHANGE_RATE',
  SET_DATE = 'SET_DATE',
  SET_HEADER = 'SET_HEADER',
  SET_MONEY = 'SET_MONEY',
}

export default ActionTypes;