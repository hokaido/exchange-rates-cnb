declare module store {
  interface Rate {
    country?: string;
    currency?: string;
    amount?: number;
    code?: string;
    rate?: number;
    [key: string]: string | number;
  }

  interface Action {
    type: string;
    value: Rate | Rate[] | string | string[];
  }
}