
import ActionTypes from '../actionTypes';

export interface AppState {
  rates: store.Rate[];
  choosenRate: string;
  date: string;
  tableHeader: string[];
  money: string;
}

const initialState: AppState = {
  rates: [],
  choosenRate: '',
  date: '',
  tableHeader: [''],
  money: '',
}

const mainReducer = (state: AppState = initialState, action: store.Action) => {

  switch(action.type){
    case ActionTypes.LOAD_RATES:
      return {
        ...state,
        rates: action.value
      }
    case ActionTypes.CHANGE_RATE:
      return {
        ...state,
        choosenRate: action.value
      }
    case ActionTypes.SET_DATE:
      return {
        ...state,
        date: action.value,
      }
    case ActionTypes.SET_HEADER:
      return {
        ...state,
        tableHeader: action.value,
      }
    case ActionTypes.SET_MONEY:
      return {
        ...state,
        money: action.value,
      }
    default:
      return state;
  }
}

export default mainReducer;