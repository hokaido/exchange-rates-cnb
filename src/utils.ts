
export const parseRates = (values: string): {date: string, header: string[], data: store.Rate[]} => {
  
  const splitted = values.split('\n');

  const date = splitted.shift().split('#')[0];
  const header = splitted.shift().split('|');
  const data = splitted.map((item: string) => {
    if (item.length === 0) {
      return null;
    }
    const itemSplitted = item.split('|');
    return {
      country: itemSplitted.shift(),
      currency: itemSplitted.shift(),
      amount: parseInt(itemSplitted.shift()),
      code: itemSplitted.shift(),
      rate: parseFloat(itemSplitted.shift().replace(',', '.'))
    }
  })

  return {
    date,
    header,
    data
  }
}